import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { EditorComponent } from './editor/editor.component';


const routes: Routes = [
  { path: "", pathMatch: "full", component: WelcomeComponent },
  { path: "editor", component: EditorComponent },
  { path: "**", redirectTo: "" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
