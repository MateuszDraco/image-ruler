import Konva from 'konva';
import { Point } from './Point';

export class Line {
  readonly start: Point;
  readonly end: Point;

  private static readonly selectedColor = 'green';
  private static readonly unselectedColor = 'blue';

  private _ratio: number = 1;
  private readonly lineObj: Konva.Line;
  private readonly lineText = new Konva.Text({
    fontSize: 12,
    fontFamily: 'Roboto',
    fill: 'red',
    strokeWidth: 10
  });
  constructor(start: Point, end: Point, layer: Konva.Layer, ratio: number, onClick: (Line) => void) {
    this.start = start;
    this.end = end;
    this.lineObj = new Konva.Line({
      points: [start.x, start.y, end.x, end.y],
      listening: true,
      stroke: Line.unselectedColor,
      strokeWidth: 2
    });
    this.lineObj.on('click', () => {
      onClick(this);
    });
    this.lineObj.on('mouseenter', () => {
      this.lineObj.strokeWidth(4);
      this.lineObj.draw();
      this.lineText.fontStyle('bold');
      this.lineText.draw();
    });
    this.lineObj.on('mouseleave', () => {
      this.lineObj.strokeWidth(2);
      this.lineText.fontStyle('normal');
      layer.draw();
    });
    this.ratio = ratio;
    this.lineText.x((start.x + end.x - this.lineText.width()) / 2);
    this.lineText.y((start.y + end.y - this.lineText.height()) / 2);
    this.bind(layer);
  }
  set ratio(ratio: number) {
    this._ratio = ratio;
    this.lineText.text(`${this.realLeangth}`);
  }
  get ratio() {
    return this._ratio;
  }
  get screenLength() {
    return ((this.start.x - this.end.x) ** 2 + (this.start.y - this.end.y) ** 2) ** 0.5;
  }
  get realLeangth() {
    return Math.round(this.ratio * this.screenLength);
  }

  set selected(isSelected) {
    this.lineObj.stroke(isSelected ? Line.selectedColor : Line.unselectedColor);
  }

  bind(layer: Konva.Layer) {
    layer.add(this.lineText);
    layer.add(this.lineObj);
  }

  unbind() {
    this.lineText.destroy();
    this.lineObj.destroy();
  }
}
