import { Component, OnInit } from '@angular/core';
import Konva from 'konva';
import { Point } from './Point';
import { Line } from './Line';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit {

  private stage: Konva.Stage;
  private readonly image = new Image();
  private imageLayer: Konva.Layer;
  private lineStart: Point;

  private readonly linesLayer = new Konva.Layer();
  private ratio: number = 1;
  private lines: Line[] = [];

  private _selectedLine: Line;
  
  private readonly activeLayer: Konva.Layer = new Konva.Layer({
    clearBeforeDraw: true
  });
  
  private readonly activeLine = new Konva.Line({
    points: [],
    stroke: 'red'
  });

  constructor() { 
  }

  ngOnInit() {
    let width = window.innerWidth;
    let height = window.innerHeight;

    this.stage = new Konva.Stage({
      container: 'konva',
      width: width,
      height: height
    });

    let image = new Konva.Image({
      image: this.image,
      x: 50,
      y: 150,
    });
    this.imageLayer = new Konva.Layer();
    this.imageLayer.add(image);
    this.image.onload = () => {
      this.clear();
      this.imageLayer.draw();
    };
    this.stage.add(this.imageLayer);

    this.activeLayer.add(this.activeLine);
    this.stage.add(this.activeLayer);

    this.stage.add(this.linesLayer);
    
    this.stage.on('mousedown', () => {
      let pos = this.stage.getPointerPosition();
      this.lineStart = pos;
    });
    this.stage.on('mouseup', () => {
      let pos = this.stage.getPointerPosition();
      if (this.lineStart.x != pos.x || this.lineStart.y != pos.y) {
        let line = this.addLine(this.lineStart, pos);
        this.selectedLine = line;
      } else {
        this.selectedLine = null;
      }
      this.lineStart = null;
      this.activeLine.points([]);
      this.activeLayer.clear();
      this.activeLayer.draw();
    });
    this.stage.on('mousemove', () => {
      if (this.lineStart != null) {
        let pos = this.stage.getPointerPosition();
        let points = [this.lineStart.x, this.lineStart.y, pos.x, pos.y];
        this.activeLine.points(points);
        this.activeLayer.draw();
      }
    });
  }

  onLineClick1(line: Line) {
    let newLenght = prompt('Enter true line length', `${Math.round(this.ratio * line.screenLength)}`);
  }

  onLineClick(line: Line) {
    this.selectedLine = line;
  }

  setSelectedWidth(newLenght: string) {
    if (newLenght != null) {
      let nl = Number.parseFloat(newLenght);
      if (!isNaN(nl) && nl > 0) {
        this.updateRatio(nl / this.selectedLine.screenLength);
      } else {
        console.warn(`wrong input: {${newLenght}}`);
      }
    }
  }

  addLine(start: Point, end: Point): Line {
    let line = new Line(start, end, this.linesLayer, this.ratio, l => this.onLineClick(l));
    this.lines.push(line);

    this.linesLayer.draw();
    return line;
  }

  updateRatio(ratio: number) {
    this.ratio = ratio;
    this.lines.forEach(line => { line.ratio = ratio; });
    this.linesLayer.batchDraw();
  }

  openFile() {
    let url = prompt('Enter image url:');
    if (url != null && url.length > 0) {
      this.clear();
      this.image.src = url;
    }
  }

  clear(withImage: boolean = true) {
    if (withImage) {
      this.imageLayer.clear();
    }
    this.selectedLine = null;
    this.lines = [];
    this.linesLayer.removeChildren();
    this.linesLayer.destroyChildren();
    this.linesLayer.batchDraw();
  }

  get selectedLine() {
    return this._selectedLine;
  }

  set selectedLine(line: Line) {
    if (this._selectedLine != null) {
      this._selectedLine.selected = false;
    }
    this._selectedLine = line;
    if (line != null) {
      line.selected = true;
    }
    this.linesLayer.draw();
  }

  removeSelected() {
    if (this.selectedLine != null) {
      this.lines = this.lines.filter(x => x != this.selectedLine);
      this.selectedLine.unbind();
      this.selectedLine = null;
    }
  }
}
